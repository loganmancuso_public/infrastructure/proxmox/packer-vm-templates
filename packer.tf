##############################################################################
#
# Author: Logan Mancuso
# Created: 11.28.2023
#
##############################################################################

locals {
  template_name = "mi-${var.image.name}-${var.image.version}"
  # variables passed to packer
  file_name_suffix = "${var.image.name}-${var.image.version}.json"
  # where all the source tmpl files will come from, this holds the scructure of the 
  # folders to be copied to the target dir
  tmpl_source = "${path.module}/templates/${var.image.os}"
  # where all the transformed tmpl files will be copied to
  # the path needs to include the cluster name (var.config.env) to keep the path unique
  # files named for the node can share a common name, primary.* and worker01.* ect
  tmpl_dest = "${path.module}/.transformed/${var.image.os}/${var.config.env}"
  # Directories configured for the instance
  directories = {
    home = "/opt/tofu"     # default home directory
    log  = "/var/log/tofu" # default log directory 
  }
}

# user-data file is shared across all deployments of the same os
resource "local_file" "user_data" {
  filename = "${local.tmpl_dest}/http/user-data"
  content = templatefile("${local.tmpl_source}/http/user-data.tmpl",
    {
      hostname        = local.template_name
      username        = local.credentials_instance.username
      authorized_key  = local.credentials_instance.sshkey_public
      hashed_password = local.credentials_instance.hashed_password
      password        = local.credentials_instance.password
  })
}

# init.sh file is shared across all deployments of the same os
resource "local_file" "init" {
  filename = "${local.tmpl_dest}/files/init.sh"
  content = templatefile("${local.tmpl_source}/files/init.tmpl",
    {
      log           = "${local.directories.log}/init.log"
      directories   = local.directories
      id            = local.template_name
      username      = local.credentials_instance.username
      resources_key = base64encode(local.credentials_instance.sshkey_resource_private)
  })
}

resource "local_file" "packer_variables" {
  for_each = { for idx, key in keys(local.nodes) : key => idx }
  filename = "${local.tmpl_dest}/${each.key}.${local.file_name_suffix}"
  content = jsonencode({
    config = {
      env                 = var.config.env
      node_name           = local.nodes[each.key].name
      node_ip             = local.nodes[each.key].ip
      port                = 8800 + each.value
      vlan                = local.networks[var.image.network].vlan == 0 ? null : local.networks[var.image.network].vlan
      id                  = "${local.prefix}${var.image.id + each.value}"
      username            = local.credentials_instance.username
      iso                 = local.images[each.key][var.image.os][var.image.name].id
      os                  = var.image.os
      name                = local.template_name
      description         = var.image.description
      authorized_key_path = "/${local.credentials_proxmox.username}/.ssh/instance_key" # this key is copied to the node in the datacenter bootstrap
      directories         = local.directories
    }
  })
}

# all the files that are not .tmpl files will be copied to the tmpl_dest
resource "terraform_data" "copy_static_files" {
  depends_on = [local_file.user_data, local_file.init, local_file.packer_variables]
  provisioner "local-exec" {
    when        = create
    working_dir = path.module
    command     = "rsync -av --exclude='*.tmpl' ${local.tmpl_source}/ ${local.tmpl_dest}/"
  }
}

locals {
  packer_destroy = {
    for idx, key in keys(local.nodes) :
    key => "${local.prefix}${var.image.id + idx}"
  }
}

resource "terraform_data" "packer_deploy" {
  for_each         = local.nodes
  triggers_replace = [local_file.user_data.content_md5, local_file.packer_variables, md5(file("${local.tmpl_source}/main.pkr.hcl"))]
  depends_on       = [terraform_data.copy_static_files]
  input = {
    nodes            = local.nodes
    proxmox_username = local.credentials_proxmox.username
    sshkey_path      = local.credentials_proxmox.sshkey_path
    # calculate vm id for destroy
    packer_destroy = { for idx, key in keys(local.nodes) : key => "${local.prefix}${var.image.id + idx}" }
  }
  # Create Packer Folders
  provisioner "remote-exec" {
    when = create
    inline = [
      "[ -d \"${local.directories.home}/packer/templates/${var.image.os}\" ] && rm -rf \"${local.directories.home}/packer/templates/${var.image.os}\"",
      "mkdir -p \"${local.directories.home}/packer/templates/${var.image.os}\""
    ]
    connection {
      type        = "ssh"
      user        = local.credentials_proxmox.username
      private_key = file(local.credentials_proxmox.sshkey_path)
      host        = local.nodes[each.key].ip
    }
  }
  # Copy Transformed Packer Files to Target 
  provisioner "file" {
    when        = create
    source      = "${local.tmpl_dest}/" # trailing slash to copy contents 
    destination = "${local.directories.home}/packer/templates/${var.image.os}"
    connection {
      type        = "ssh"
      user        = local.credentials_proxmox.username
      private_key = file(local.credentials_proxmox.sshkey_path)
      host        = local.nodes[each.key].ip
    }
  }
  # Run Packer Deploy on Transformed Files
  provisioner "remote-exec" {
    when = create
    inline = [
      "export PROXMOX_USERNAME='${local.credentials_operations.api_id}'",
      "export PROXMOX_TOKEN='${local.credentials_operations.api_key[var.config.env]}'",
      "cd ${local.directories.home}/packer/templates/${var.image.os}",
      "packer init main.pkr.hcl",
      "packer validate -var-file=${each.key}.${local.file_name_suffix} main.pkr.hcl",
      "packer build -var-file=${each.key}.${local.file_name_suffix} main.pkr.hcl"
    ]
    connection {
      type        = "ssh"
      user        = local.credentials_proxmox.username
      private_key = file(local.credentials_proxmox.sshkey_path)
      host        = local.nodes[each.key].ip
    }
  }
  # Run Packer Deploy on Transformed Files
  provisioner "remote-exec" {
    when = destroy
    inline = [
      "echo destroy vm id: ${self.input.packer_destroy[each.key]}",
      "qm destroy ${self.input.packer_destroy[each.key]}"
    ]
    connection {
      type        = "ssh"
      user        = self.input.proxmox_username
      private_key = file(self.input.sshkey_path)
      host        = self.input.nodes[each.key].ip
    }
  }
}

# TODO future support for appending tags to an existing resource.
# data "proxmox_virtual_environment_vm" "instance" {
#   for_each  = { for idx, key in keys(local.nodes) : key => idx }
#   node_name = local.nodes[each.key].name
#   vm_id     = "${local.prefix}${var.image.id + each.value}"
#   tags      = concat([var.image.os, "${var.image.name}-${var.image.version}"], var.image.tags)
# }