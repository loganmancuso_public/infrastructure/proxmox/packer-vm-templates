##############################################################################
#
# Author: Logan Mancuso
# Created: 07.30.2023
#
# Packer Template to create an Ubuntu Server on Proxmox
##############################################################################

## Provider ##
packer {
  required_version = ">= 1.11.2"
  required_plugins {
    proxmox = {
      version = ">= 1.2.2"
      source  = "github.com/hashicorp/proxmox"
    }
  }
}

## Variable Definitions ##
variable "config" {
  description = "required packer parameters"
  type = object({
    env                    = string
    node_name              = string
    node_ip                = string
    port                   = string
    vlan                   = number
    id                     = string
    username               = string
    iso                    = string
    os                     = string
    name                   = string
    description            = string
    authorized_key_path    = string
    directories            = map(string)
  })
}

#######################################
# Resource Definiation
# VM Template
#######################################
source "proxmox-iso" "template" {

  # Proxmox Connection Settings
  proxmox_url              = "https://${var.config.node_ip}:8006/api2/json"
  insecure_skip_tls_verify = true

  # PACKER Autoinstall Settings
  http_directory = "${path.cwd}/http"
  # this port range is permitted in the datacenter infrastructure firewall and through the network firewall
  http_port_min = var.config.port
  http_port_max = var.config.port

  # VM General Settings
  node                 = var.config.node_name
  vm_id                = var.config.id
  vm_name              = var.config.name
  template_description = var.config.description
  os                   = "l26"
  bios                 = "seabios"

  boot_iso {
    type      = "scsi"
    iso_file  = var.config.iso
    unmount   = true
  }

  # VM System Settings
  qemu_agent = true

  # VM Hard Disk Settings
  scsi_controller = "virtio-scsi-pci"

  disks {
    disk_size    = "10G"
    format       = "raw"
    storage_pool = "local-lvm"
    type         = "virtio"
  }

  # VM CPU Settings
  cores = "2"

  # VM Memory Settings
  memory = "2048"

  # VM Network Settings
  network_adapters {
    model    = "virtio"
    bridge   = "vmbr0"
    firewall = "false"
    vlan_tag = var.config.vlan
  }

  # VM Cloud-Init Settings
  cloud_init              = true
  cloud_init_storage_pool = "local-lvm"

  # PACKER Boot Commands
  boot_command = ["c<wait2s>linux /casper/vmlinuz --- autoinstall ds='nocloud-net;s=http://${var.config.node_ip}:${var.config.port}/'<enter><wait5s>initrd /casper/initrd <wait2s><enter><wait2s>boot <enter>"]
  boot         = "c"
  boot_wait    = "8s"

  ssh_username    = var.config.username
  ssh_private_key_file = var.config.authorized_key_path

  # Raise the timeout, when installation takes longer
  ssh_timeout = "30m"
}

#######################################
# Build Definition
# create the VM Template
#######################################
build {

  name    = var.config.name
  sources = ["source.proxmox-iso.template"]

  #Cloud-Init Integration in Proxmox #
  provisioner "shell" {
    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done",
      "export DEBIAN_FRONTEND=\"noninteractive\"",
      "sudo rm /etc/ssh/ssh_host_*",
      "sudo truncate -s 0 /etc/machine-id",
      "sudo apt -y autoremove --purge",
      "sudo apt -y clean",
      "sudo apt -y autoclean",
      "sudo cloud-init clean",
      "sudo rm -f /etc/cloud/cloud.cfg.d/subiquity-disable-cloudinit-networking.cfg",
      "sudo sync",
      # configure default logging paths #
      "sudo mkdir -p ${var.config.directories.log}",
      "sudo chown -R root:${var.config.username} ${var.config.directories.log}",
      "sudo chmod -R u+rwx,g+rwx ${var.config.directories.log}",
      "sudo mkdir -p ${var.config.directories.home}",
      "sudo chown -R root:${var.config.username} ${var.config.directories.home}",
      "sudo chmod -R u+rwx,g+rwx ${var.config.directories.home}"
    ]
  }
  provisioner "file" {
    source      = "${path.cwd}/files/99-pve.cfg"
    destination = "/tmp/99-pve.cfg"
  }
  provisioner "shell" {
    inline = ["sudo cp /tmp/99-pve.cfg /etc/cloud/cloud.cfg.d/99-pve.cfg"]
  }

  provisioner "file" {
    source      = "${path.cwd}/files/init.sh"
    destination = "${var.config.directories.home}/init.sh"
  }

  provisioner "shell" {
    inline = ["chmod +x ${var.config.directories.home}/init.sh && bash ${var.config.directories.home}/init.sh"]
  }
}
