##############################################################################
#
# Author: Logan Mancuso
# Created: 06.20.2024
#
##############################################################################

variable "config" {
  description = "default settings and parameters for the environment"
  type = object({
    env = string
  })
}

variable "image" {
  description = "configurations for the image"
  type = object({
    os          = string
    name        = string
    version     = string
    description = string
    id          = number
    network     = string
    tags        = list(string)
  })
}