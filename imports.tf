##############################################################################
#
# Author: Logan Mancuso
# Created: 06.20.2024
#
##############################################################################

data "terraform_remote_state" "datacenter" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/${var.config.env}"
  }
}

data "terraform_remote_state" "global_secrets" {
  backend = "http"
  config = {
    address = "https://gitlab.com/api/v4/projects/XXXXXXXX/terraform/state/global-secrets"
  }
}

locals {
  # datacenter
  nodes    = data.terraform_remote_state.datacenter.outputs.nodes
  prefix   = data.terraform_remote_state.datacenter.outputs.resource_prefix.template_instance
  networks = data.terraform_remote_state.datacenter.outputs.networks
  images   = data.terraform_remote_state.datacenter.outputs.images_machine
  # global_secrets
  secret_proxmox    = data.terraform_remote_state.global_secrets.outputs.proxmox
  secret_instance   = data.terraform_remote_state.global_secrets.outputs.instance
  secret_operations = data.terraform_remote_state.global_secrets.outputs.user_operations
}

## Obtain Vault Secrets ##
data "vault_kv_secret_v2" "proxmox" {
  mount = local.secret_proxmox.mount
  name  = local.secret_proxmox.name
}

data "vault_kv_secret_v2" "instance" {
  mount = local.secret_instance.mount
  name  = local.secret_instance.name
}

data "vault_kv_secret_v2" "operations" {
  mount = local.secret_operations.mount
  name  = local.secret_operations.name
}

locals {
  credentials_proxmox = jsondecode(data.vault_kv_secret_v2.proxmox.data_json)
  # These values are sensitive but I override this so I can see the output of the cloudinit and bootstrap
  credentials_instance   = nonsensitive(jsondecode(data.vault_kv_secret_v2.instance.data_json))
  credentials_operations = nonsensitive(jsondecode(data.vault_kv_secret_v2.operations.data_json))
}