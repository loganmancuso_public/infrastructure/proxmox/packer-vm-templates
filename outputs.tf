##############################################################################
#
# Author: Logan Mancuso
# Created: 06.20.2024
#
##############################################################################

output "templates" {
  description = "the deployed vm template in proxmox"
  value = { for idx, key in keys(local.nodes) :
    key => {
      id   = "${local.prefix}${var.image.id + idx}"
      tags = concat([var.image.os, "${var.image.name}-${var.image.version}"], var.image.tags)
    }
  }
}

output "directories" {
  description = "home and log directories built into the instance"
  value       = local.directories
}

## Enable for Debugging as needed ##
# output packer_commands {
#     value = [
#     "export PROXMOX_USERNAME=${local.credentials_operations.api_id}",
#     "export PROXMOX_TOKEN=${local.credentials_operations.api_key[var.config.env]}",
#     "cd ${local.directories.home}/packer/templates/${var.image.os}",
#     "packer init main.pkr.hcl",
#     "packer validate -var-file=primary.${local.file_name_suffix} main.pkr.hcl",
#     "packer build -var-file=primary.${local.file_name_suffix} main.pkr.hcl"
#   ]
# }